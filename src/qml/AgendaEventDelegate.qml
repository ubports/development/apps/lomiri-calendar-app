/*
 * Copyright (C) 2024 Ubports Foundation
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Lomiri.Components 1.3

import "./3rd-party/lunar.js" as Lunar

Item {

    property var eventModel
    property var event: eventModel.items[index];
    property var prevEvent: eventModel.items[index-1];
    property var date: event.startDateTime.toLocaleDateString()
    property var startTime: event.startDateTime.toLocaleTimeString(Qt.locale(), Locale.ShortFormat)
    property var endTime: event.endDateTime.toLocaleTimeString(Qt.locale(), Locale.ShortFormat)
    property var lunarDate: Lunar.calendar.solar2lunar(event.startDateTime.getFullYear(),
                                                       event.startDateTime.getMonth() + 1,
                                                       event.startDateTime.getDate())

    width: parent.width
    height: eventContainer.height


    signal dateClicked(date startDateTime)
    signal eventClicked(var event)

    // main Column to hold   header-(date) and event details-(start/end time, Description and location)
    Column {
        id: eventContainer
        objectName: "eventContainer" + index

        width: parent.width
        anchors.top: parent.top

        // header ListItem eg. ( Friday, October 29th 2015 )
        ListItem {
            width: parent.width
            height: visible ? units.gu(4) : 0
            color: theme.palette.normal.foreground
            highlightColor: theme.palette.highlighted.background
            visible: index === 0 ? true : prevEvent === undefined ? false : prevEvent.startDateTime.midnight() < event.startDateTime.midnight() ? true : false

            ListItemLayout {
                id: listitemlayout
                padding.top: units.gu(1)
                title.text: mainView.displayLunarCalendar ? ("%1 %2 %3 %4 %5").arg(lunarDate.gzYear).arg(lunarDate .IMonthCn).arg(lunarDate.IDayCn)
                                                                              .arg(lunarDate.gzDay).arg(lunarDate.isTerm ? lunarDate.Term : "")
                                                          : date
                title.color: event.startDateTime.toLocaleDateString() === new Date().toLocaleDateString() ? LomiriColors.orange : theme.palette.normal.backgroundSecondaryText
            }

            // onClicked new page with daily view will open.
            onClicked: dateClicked(event.startDateTime)
        }

        // Main ListItem to hold details about event eg. ( 19:30 -   Beer with the team )
        //                                               ( 20:00     Hicter             )
        ListItem {
            id: detailsListItem

            width: parent.width
            height: detailsListitemlayout.height
            color: theme.palette.normal.background
            highlightColor: theme.palette.highlighted.background

            ListItemLayout {
                id: detailsListitemlayout

                title.font.bold: true
                title.text: event.displayLabel ? event.displayLabel : i18n.tr("no event name set")
                subtitle.font.pixelSize: title.font.pixelSize
                subtitle.text: event.location ? event.location : i18n.tr("no location")
                subtitle.color: event.location ? theme.palette.normal.backgroundSecondaryText : theme.palette.disabled.backgroundText
                subtitle.font.italic: event.location ? false : true


                // item to hold SlotsLayout.Leading items: timeStart timeEnad and little calendar indication icon.
                Item {
                    width: timeLabelStart.width + units.gu(2)
                    height: parent.height
                    SlotsLayout.overrideVerticalPositioning: true
                    SlotsLayout.position: SlotsLayout.Leading

                    // Little icon in left top corner of every event to indicate color of the calendar.
                    LomiriShape {
                        id: calendarIndicator

                        anchors.verticalCenter: parent.verticalCenter
                        width: units.gu(1)
                        height: width
                        aspect: LomiriShape.DropShadow
                        backgroundColor: eventModel.collection(event.collectionId).color
                    }

                    // start time event Label
                    Label {
                        id: timeLabelStart

                        anchors {left: calendarIndicator.right; leftMargin: units.gu(1)}
                        fontSize: "small"
                        y: detailsListitemlayout.mainSlot.y + detailsListitemlayout.title.y
                           + detailsListitemlayout.title.baselineOffset - baselineOffset
                        text: startTime.concat('-')
                    }

                    // finish time event Label
                    Label {
                        id: timeLabelEnd

                        anchors {left: calendarIndicator.right; leftMargin: units.gu(1)}
                        fontSize: "small"
                        y: detailsListitemlayout.mainSlot.y + detailsListitemlayout.subtitle.y
                           + detailsListitemlayout.subtitle.baselineOffset - baselineOffset;
                        text: endTime
                    }
                }

            }

            // new page will open to edit selected event
            onClicked: eventClicked(event)
        }
    }
}
