/*
 * Copyright (C) 2024 Ubports Foundation
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3
import Qt.labs.platform 1.0
import QtOrganizer 5.0


import "./3rd-party/ical.js" as Parser

Page {
    id: sharePage

    property url fileUrl
    property var activeTransfer

    property alias startPeriod: organizerExporter.startPeriod
    property alias endPeriod: organizerExporter.endPeriod
    property alias filter: organizerExporter.filter

    property var partialEventsMap: new Map()

    header: PageHeader {
        title: i18n.tr("Export to")
    }

    function exportEvents() {
        const dir = StandardPaths.writableLocation(StandardPaths.CacheLocation)
        const ext = new Date().toLocaleString(Qt.locale(), "yyyyMMddHHmmss")
        fileUrl = dir + "/calendar_" + ext + ".ics"
        console.log('set export file to path:', fileUrl)

        organizerExporter.exportItems(fileUrl);
    }

    function onExportError(msg) {
        if (msg) {
            errorMsg.text = msg
        }

        errorMsg.visible = true
    }

    function onExportSuccess() {
        console.log('Exported successfully to', fileUrl)
        picker.visible = true
    }

    function completeExport(url) {

        for(var i = 0 ; i < organizerExporter.itemCount ; ++i) {
            var item = organizerExporter.items[i]
            var attendees = item.details(Detail.EventAttendee)
            let participants = []
            if (attendees) {
               participants = attendees.map( attendee => {

                    return {
                       email: attendee.emailAddress,
                       name: attendee.name
                    }
                })
            }
            partialEventsMap.set(item.guid, { location: item.location, participants: participants })
        }

        if (partialEventsMap.size === 0) {
            return onExportSuccess(url)
        }


        let request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    completeICS(request.responseText)
                } else {
                    console.error('could not read ICS file from', url)
                    onExportError()
                }
            }
        }
        request.open("GET", url)
        request.send();
    }

    function completeICS(ics) {
        try {
            var jcalData = Parser.ICAL.parse(ics);
            let component = new Parser.ICAL.Component(jcalData);

            const vevents = component
                .getAllSubcomponents('vevent');

            vevents.forEach( vevent => {

                var uid = vevent.getFirstPropertyValue('uid');
                var partialEvent = partialEventsMap.get(uid)

                if (partialEvent) {
                    if (partialEvent.location && partialEvent.location.length > 0) {
                        vevent.addPropertyWithValue('location', partialEvent.location);
                    }
                    if (partialEvent.participants && partialEvent.participants.length > 0) {

                        partialEvent.participants.forEach( participant => {
                            var property = vevent.addPropertyWithValue('attendee', participant.email);
                            property.setParameter('cn', participant.name);
                            property.setParameter('partstat', "TENTATIVE");
                            property.setParameter('role',"REQ-PARTICIPANT");
                            property.setParameter('rsvp',"FALSE");
                        })
                    }
                }
            });

            // export new file
            var request = new XMLHttpRequest();

            request.onreadystatechange = function() {
                if (request.readyState === XMLHttpRequest.DONE) {
                    console.log('PUT status', request.status)
                    onExportSuccess()
                }
            }
            request.open("PUT", fileUrl);
            request.send(component.toString());
        } catch (e) {
            onExportError()
            console.error('ICAL parsing error:', e);
        }
    }


    OrganizerModel{
        id: organizerExporter
        manager:"eds"
        autoUpdate: true

        onItemCountChanged: {
            if (itemCount > 0) {
                console.log('itemCount', itemCount)
                exportEvents();
            } else {
                console.log('humm, export model is empty!!???')
                onExportError(i18n.tr('no events found!'));
            }
        }

        onExportCompleted: {
            if (error === 0) {
                completeExport(url);
            } else {
                console.log('error on export:', error)
                onExportError();
            }
        }
    }

    Component {
        id: resultComponent
        ContentItem {}
    }

    ContentPeerPicker {
        id: picker
        anchors.topMargin: sharePage.header.height
        visible: false
        handler: ContentHandler.Destination
        contentType: ContentType.Documents
        showTitle: false

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single;
            sharePage.activeTransfer = peer.request();
        }

        onCancelPressed: {
            pageStack.pop();
        }
    }

    Connections {
        target: sharePage.activeTransfer ? sharePage.activeTransfer : null
        onStateChanged: {
            if (sharePage.activeTransfer && sharePage.activeTransfer.state === ContentTransfer.InProgress) {
                sharePage.activeTransfer.items = [ resultComponent.createObject(sharePage, { "url": sharePage.fileUrl }) ];
                sharePage.activeTransfer.state = ContentTransfer.Charged;
                pageStack.pop()
            }
        }
    }

    Label {
        id: errorMsg
        visible: false
        anchors.centerIn: parent
        //width: parent.width - units.gu(4)
        text: i18n.tr("Oops, something went wrong during export, please check the logs")
        wrapMode: Label.WordWrap
    }

    ContentTransferHint {
        anchors.fill: parent
        activeTransfer: sharePage.activeTransfer
    }
}
