/*
 * Copyright (C) 2024 Ubports Foundation
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtOrganizer 5.0
import Qt.labs.platform 1.0

import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3
import "dateExt.js" as DateExt
import "./3rd-party/ical.js" as Parser

Page {
    id: importPage

    property var files: []
    property var model
    property var dtFilter: new Date()

    property var partialEventsMap: new Map()
    property bool importFinished: false

    signal finalized()

    function onImportError(msg) {
        PopupUtils.open(errorDialog, importPage, {"text": msg})
    }

    function onImportSuccess() {
        PopupUtils.open(successDialog, importPage, {"text": i18n.tr("Successfully imported %1 event", "Successfully imported %1 events", organizerImporter.itemCount).arg(organizerImporter.itemCount)})
    }

    function startImport(file) {

        if (!file || file.length ===0) {
            console.error('humm, no file provided...')
            return
        }

        // we use ical.js since "Location" and "attendees" import is not supported by QtOrganizer
        // we use QtOrganizer still for common fields and recurrence management at the end of that process
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    parseICS(request.responseText)
                    organizerImporter.importItems(file);
                } else {
                    console.error('could not read ICS file from', url)
                    onExportError(i18n.tr('Error while parsing Ical file'))
                }
            }
        }
        request.open("GET", file);
        request.send(null);
    }

    function parseICS(ics) {
        try {

            var jcalData = Parser.ICAL.parse(ics);
            let component = new Parser.ICAL.Component(jcalData);

            const vevents = component.getAllSubcomponents('vevent');

            vevents.forEach( vevent => {

                var uid = vevent.getFirstPropertyValue('uid');
                var location = vevent.getFirstPropertyValue('location');
                var attendees = vevent.getAllProperties('attendee')
                const participants = attendees.map( attendee => {

                   var attendeeEmail = attendee.getFirstValue();
                   if (!attendeeEmail) {
                       return;
                   }

                   const cn = attendee.getParameter('cn')
                   const mail = attendeeEmail.replace(/^mailto:/i, '');

                   return {
                       email: mail,
                       name: cn || mail
                   }
               })

                partialEventsMap.set(uid, { location: location, attendees: participants })

            });

        } catch (e) {
            console.error(e);
            onImportError(i18n.tr("Error during import, please check the logs"))
        }
    }

    function completeEvent(item) {

        const partialEvent = partialEventsMap.get(item.guid)
        let updated = false
        if (partialEvent) {

            if (partialEvent.location && partialEvent.location.length > 0) {
                item.location = partialEvent.location
                updated = true
            }

            if (partialEvent.attendees && partialEvent.attendees.length > 0) {

                partialEvent.attendees.forEach( attendee => {
                   let eAttendee = Qt.createQmlObject("import QtOrganizer 5.0; EventAttendee { }", item, "")
                   eAttendee.emailAddress = attendee.email
                   eAttendee.name = attendee.name
                   eAttendee.participationRole = EventAttendee.RoleOptionalParticipant
                   eAttendee.participationStatus = EventAttendee.StatusUnknown
                   item.setDetail(eAttendee)
                   updated = true
                });
            }
        }

        if (updated) {
            organizerImporter.saveItem(item)
        }
    }

    function importEvents(collectionId){

        console.log('Import events to collection:', collectionId)
        for(var i = 0 ; i < organizerImporter.itemCount ; ++i) {
            var item = organizerImporter.items[i]

            // unfortunately copy of event from a collection to another is not possible, so recreate a full one
            var event = Qt.createQmlObject(
                "import QtOrganizer 5.0; Event {}",
                Qt.application,
                "ImportEventsDialog_importEvents");
            event.collectionId = collectionId;
            event.allDay = item.allDay;
            event.startDateTime = new Date(item.startDateTime);
            event.endDateTime = new Date(item.endDateTime);
            event.displayLabel = item.displayLabel;
            event.description = item.description;
            event.location = item.location;

            var reminder = item.detail(Detail.VisualReminder)
            if (reminder) {
                var newReminder = Qt.createQmlObject("import QtOrganizer 5.0; VisualReminder {}", event, "")
                newReminder.repetitionCount = 0
                newReminder.repetitionDelay = 0
                newReminder.message = item.displayLabel
                newReminder.secondsBeforeStart = reminder.secondsBeforeStart
                event.setDetail(newReminder)
            }

            var attendees = item.details(Detail.EventAttendee)
            if (attendees) {
                attendees.forEach( attendee => {
                    let newAttendee = Qt.createQmlObject("import QtOrganizer 5.0; EventAttendee { }", event, "")
                    newAttendee.emailAddress = attendee.emailAddress
                    newAttendee.name = attendee.name
                    newAttendee.participationRole = attendee.participationRole
                    newAttendee.participationStatus = attendee.participationStatus
                    event.setDetail(newAttendee)
                })
            }


            model.saveItem(event)
        }
        onImportSuccess()
    }

    header: PageHeader {
        id: header
        title: i18n.tr("Import events")
        leadingActionBar.actions: Action {
            id: backAction

            name: "cancel"
            text: i18n.tr("Cancel")
            iconName: "back"
            onTriggered: importPage.finalized()
        }
        trailingActionBar.actions: [
            Action {
                iconName: "save"
                objectName: "save"
                text: i18n.tr("Save")
                enabled: organizerImporter.itemCount > 0
                onTriggered: {
                    var newCollection = calendarsOption.model[calendarsOption.selectedIndex].collectionId;
                    importEvents(newCollection)
                }
            }
        ]

        // flickable: flickable
        flickable: null
    }

    OrganizerModel{
        id: organizerImporter
        manager:"memory"
        autoUpdate: true
        startPeriod: dtFilter.midnight();
        endPeriod: dtFilter.addYears(2).endOfDay()

        Component.onCompleted : {
            if (managerName == "memory") {
                console.log('importing... from file', files[0])
                importPage.importFinished = false
                importPage.startImport(files.shift());
            }
        }

        onImportCompleted: {
            console.log('import completed', itemCount)
            if (error !==0) {
                console.log('Error during import code:', error)
                // error
                onImportError(i18n.tr("Error during import, is it an iCalendar file ?"))
            } else {
                if (ids.length === 0) {
                    onImportError(i18n.tr("No events found!"))
                } else {
                    if (itemCount === 0) {
                        onImportError(i18n.tr("No events found!, could not import events"))
                        return
                    } 

                    for (var i=0; i < itemCount; i++) {
                        importPage.completeEvent(items[i])
                    }
                    // any other files to import ?
                    const file = files.shift()
                    if (file) {
                        importPage.startImport(file);
                    } else {
                        importPage.importFinished = true
                    }
                }
            }
        }

        onModelChanged: console.log('model change', itemCount)
    }

    OptionSelector{
        id: calendarsOption
        objectName: "calendarsOption"

        anchors {
            top: header.bottom
            margins: units.gu(2)
            left: parent.left
            right: parent.right
        }
        text: i18n.tr("To calendar:")
        visible: importPage.importFinished
        selectedIndex: -1

        function init() {
            calendarsOption.model =  importPage.model ? importPage.model.getWritableAndSelectedCollections() : []
            const collectionId = importPage.model.getDefaultCollection() ? importPage.model.getDefaultCollection().collectionId : ""
            var index = 0;
            for(var i=0; i < calendarsOption.model.length; ++i){
                if(calendarsOption.model[i].collectionId === collectionId){
                    index = i;
                    break;
                }
            }
            calendarsOption.selectedIndex = index
        }

        delegate: OptionSelectorDelegate{
            text: modelData.name

            LomiriShape{
                id: calColor
                width: height
                height: parent.height - units.gu(2)
                color: modelData.color
                anchors {
                    right: parent.right
                    rightMargin: units.gu(4)
                    verticalCenter: parent.verticalCenter
                }
            }
        }
        onExpandedChanged: Qt.inputMethod.hide();

        Connections {
            target: organizerImporter
            onModelChanged: calendarsOption.init()
        }


    }

    ListView {
        id: eventList

        anchors {
            top: calendarsOption.bottom
            topMargin: units.gu(2)
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true
        model: importPage.importFinished ? organizerImporter : null // prevent TypeError when model is updated

        delegate: AgendaEventDelegate {
            eventModel: organizerImporter
        }
    }

    // Scrollbar
    Scrollbar{
        flickableItem: eventList
        align: Qt.AlignTrailing
    }

    ActivityIndicator {
        anchors.centerIn: parent
        running: !importPage.importFinished
    }

    Component {
        id: errorDialog

        Dialog {
            id: dialogue

            title: i18n.tr("Import error")
            text: ""

            Button {
                text: i18n.tr("OK")
                onClicked:  {
                    PopupUtils.close(dialogue)
                    importPage.finalized()
                }
            }
        }
    }

    Component {
        id: successDialog

        Dialog {
            id: dialogue

            title: i18n.tr("Import completed")
            text: ""

            Button {
                text: i18n.tr("OK")
                onClicked:  {
                    PopupUtils.close(dialogue)
                    importPage.finalized()
                }
            }
        }
    }
}
