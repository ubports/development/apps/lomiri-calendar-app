/*
 * Copyright (C) 2013-2014 Canonical Ltd
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: dialogue
    objectName: "deleteConfirmationDialog"

    property var event;

    signal deleteEvent(var eventId);

    title: event.parentId ?
               i18n.tr("Delete Recurring Event"):
               i18n.tr("Delete Event") ;

    text: event.parentId ?
              // TRANSLATORS: argument (%1) refers to an event name.
              i18n.tr("Delete only this event \"%1\", or all events in the series?").arg(event.displayLabel):
              i18n.tr("Are you sure you want to delete the event \"%1\"?").arg(event.displayLabel);

    Button {
        text: i18n.tr("Delete series")
        color: theme.palette.normal.negative
        onClicked: {
            dialogue.deleteEvent(event.parentId);
            PopupUtils.close(dialogue)
        }
        visible: event.parentId !== undefined
    }

    Button {
        objectName: "deleteEventButton"
        text: event.parentId ? i18n.tr("Delete this") : i18n.tr("Delete")
        color: event.parentId ?
                  theme.name == "Lomiri.Components.Themes.SuruDark" ?
                  LomiriColors.ash : LomiriColors.graphite
                  : theme.palette.normal.negative
        onClicked: {
            dialogue.deleteEvent(event.itemId);
            PopupUtils.close(dialogue)
        }
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: PopupUtils.close(dialogue)
    }
}
