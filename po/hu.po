# Hungarian translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-01 14:35+0000\n"
"PO-Revision-Date: 2023-04-14 17:36+0000\n"
"Last-Translator: Lundrin <berenyi.vilmos@tutanota.com>\n"
"Language-Team: Hungarian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"calendar-app/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: lomiri-calendar-app.desktop.in:8 src/qml/EventDetails.qml:358
#: src/qml/NewEvent.qml:682
msgid "Calendar"
msgstr "Naptár"

#: lomiri-calendar-app.desktop.in:9
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr "Lomiri naptár, amely szinkronizál az online fiókokkal."

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "naptár;esemény;nap;hét;év;találkozó;megbeszélés;"

#: src/qml/AgendaView.qml:50 src/qml/calendar.qml:386 src/qml/calendar.qml:567
msgid "Agenda"
msgstr "Napirend"

#: src/qml/AgendaView.qml:100
msgid "You have no calendars enabled"
msgstr "Nincs engedélyezett naptár"

#: src/qml/AgendaView.qml:100
msgid "No upcoming events"
msgstr "Nincsenek közelgő események"

#: src/qml/AgendaView.qml:112
msgid "Enable calendars"
msgstr "Naptárak engedélyezése"

#: src/qml/AllDayEventComponent.qml:89 src/qml/TimeLineBase.qml:50
msgid "New event"
msgstr "Új esemény"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: src/qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 esemény"
msgstr[1] "%1 esemény"

#. TRANSLATORS: the argument refers to the number of all day events
#: src/qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 egész napos esemény"
msgstr[1] "%1 egész napos esemény"

#: src/qml/CalendarChoicePopup.qml:47 src/qml/EventActions.qml:103
msgid "Calendars"
msgstr "Naptárak"

#: src/qml/CalendarChoicePopup.qml:49 src/qml/SettingsPage.qml:51
msgid "Back"
msgstr "Vissza"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: src/qml/CalendarChoicePopup.qml:61 src/qml/EventActions.qml:79
msgid "Sync"
msgstr "Szinkronizálás"

#: src/qml/CalendarChoicePopup.qml:61 src/qml/EventActions.qml:127
msgid "Syncing"
msgstr "Szinkronizálás"

#: src/qml/CalendarChoicePopup.qml:103
msgid "Add online Calendar"
msgstr "Online naptár hozzáadása"

#: src/qml/CalendarChoicePopup.qml:217
msgid "Import events"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:230
#, fuzzy
#| msgid "Show lunar calendar"
msgid "Export calendar"
msgstr "Holdnaptár megjelenítése"

#: src/qml/CalendarChoicePopup.qml:248
msgid "Choose from"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:283
#, fuzzy
#| msgid "Calendar"
msgid "Calendar export"
msgstr "Naptár"

#: src/qml/CalendarChoicePopup.qml:301
#, fuzzy
#| msgid "Show lunar calendar"
msgid "Select your calendar"
msgstr "Holdnaptár megjelenítése"

#: src/qml/CalendarChoicePopup.qml:344
#, fuzzy
#| msgid "Select Color"
msgid "Select all"
msgstr "Szín kiválasztása"

#: src/qml/CalendarChoicePopup.qml:361 src/qml/ColorPickerDialog.qml:55
#: src/qml/DeleteConfirmationDialog.qml:63
#: src/qml/EditEventConfirmationDialog.qml:52 src/qml/EventActions.qml:175
#: src/qml/NewEvent.qml:399 src/qml/OnlineAccountsHelper.qml:73
#: src/qml/RemindersPage.qml:88
msgid "Cancel"
msgstr "Mégse"

#: src/qml/CalendarChoicePopup.qml:368
msgid "Proceed"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:403
msgid "Unable to deselect"
msgstr "Nem sikerült a kijelölés megszüntetése"

#: src/qml/CalendarChoicePopup.qml:404
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Egy új esemény létrehozásához először legalább egy írható naptárt meg kell "
"adnia"

#: src/qml/CalendarChoicePopup.qml:406 src/qml/CalendarChoicePopup.qml:419
msgid "Ok"
msgstr "OK"

#: src/qml/CalendarChoicePopup.qml:416
msgid "Network required"
msgstr "Hálózat szükséges"

#: src/qml/CalendarChoicePopup.qml:417
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""
"Ön jelenleg offline. Online fiókok hozzáadásához hálózati kapcsolat "
"szükséges."

#: src/qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Szín kiválasztása"

#: src/qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Nincsenek névjegyek"

#: src/qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Névjegy keresése"

#: src/qml/DayView.qml:76 src/qml/MonthView.qml:51 src/qml/WeekView.qml:60
#: src/qml/YearView.qml:57
msgid "Today"
msgstr "Ma"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: src/qml/DayView.qml:129 src/qml/MonthView.qml:84 src/qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr "yyyy. MMMM"

#: src/qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Ismétlődő esemény törlése"

#: src/qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Esemény törlése"

#. TRANSLATORS: argument (%1) refers to an event name.
#: src/qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Csak ezt a(z) „%1 eseményt, vagy az összes ilyet törölni szeretné?"

#: src/qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Biztos benne, hogy törölni szeretné a(z) „%1” eseményt?"

#: src/qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Összes törlése"

#: src/qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Törölje ezt"

#: src/qml/DeleteConfirmationDialog.qml:51 src/qml/NewEvent.qml:406
msgid "Delete"
msgstr "Törlés"

#: src/qml/EditEventConfirmationDialog.qml:29 src/qml/NewEvent.qml:394
msgid "Edit Event"
msgstr "Szerkesztés"

#. TRANSLATORS: argument (%1) refers to an event name.
#: src/qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""
"Csak ezt a(z) „%1 eseményt, vagy az összes ilyet szeretné szerkeszteni?"

#: src/qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Összes szerkesztése"

#: src/qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Ennek szerkesztése"

#: src/qml/EventActions.qml:115 src/qml/SettingsPage.qml:49
msgid "Settings"
msgstr "Beállítások"

#: src/qml/EventActions.qml:137 src/qml/EventActions.qml:154
msgid "Sync error"
msgstr ""

#: src/qml/EventActions.qml:155
msgid "An error occurred during synchronisation for account %1 on server %2"
msgstr ""

#: src/qml/EventActions.qml:159
msgid "Retry sync"
msgstr ""

#: src/qml/EventActions.qml:167
msgid "Edit online account"
msgstr ""

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: src/qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: src/qml/EventDetails.qml:37 src/qml/NewEvent.qml:601
msgid "Event Details"
msgstr "Esemény részletei"

#: src/qml/EventDetails.qml:41
msgid "Share"
msgstr ""

#: src/qml/EventDetails.qml:49
msgid "Edit"
msgstr "Szerkesztés"

#: src/qml/EventDetails.qml:183 src/qml/TimeLineHeader.qml:66
#: src/qml/calendar.qml:723
msgid "All Day"
msgstr "Egész nap"

#: src/qml/EventDetails.qml:402
msgid "Attending"
msgstr "Részt vesz"

#: src/qml/EventDetails.qml:404
msgid "Not Attending"
msgstr "Nem vesz részt"

#: src/qml/EventDetails.qml:406
msgid "Maybe"
msgstr "Talán"

#: src/qml/EventDetails.qml:408
msgid "No Reply"
msgstr "Nincs válasz"

#: src/qml/EventDetails.qml:447 src/qml/NewEvent.qml:637
msgid "Description"
msgstr "Leírás"

#: src/qml/EventDetails.qml:474 src/qml/NewEvent.qml:872
#: src/qml/NewEvent.qml:889
msgid "Reminder"
msgstr "Emlékeztető"

#: src/qml/EventListModel.qml:99
msgid "Birthdays & Anniversaries"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: src/qml/EventRepetition.qml:40 src/qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "Ismétlődés"

#: src/qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "Ismétlődik:"

#: src/qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr "Ismétlődések száma"

#: src/qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "Ismétlődő esemény vége"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: src/qml/EventRepetition.qml:294 src/qml/NewEvent.qml:845
msgid "Repeats"
msgstr "Ismétlődés"

#: src/qml/EventRepetition.qml:320 src/qml/NewEvent.qml:499
msgid "Date"
msgstr "Dátum"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: src/qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1, %2 alkalommal"
msgstr[1] "%1, %2 alkalommal"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: src/qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1, eddig: %2"

#: src/qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; %1 naponta"

#: src/qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; %1 hetente"

#: src/qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; %1 havonta"

#: src/qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; %1 évente"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: src/qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Hetente: %1"

#: src/qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Soha"

#: src/qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "X db előfordulás után"

#: src/qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Dátum után"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: src/qml/MonthComponent.qml:294
msgid "Wk"
msgstr "Hét"

#: src/qml/MonthView.qml:79 src/qml/WeekView.qml:140
msgid "%1 %2"
msgstr "%2 %1"

#: src/qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "A záródátumnak a kezdődátum utánra kell esnie."

#: src/qml/NewEvent.qml:394 src/qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "Új esemény"

#: src/qml/NewEvent.qml:424
msgid "Save"
msgstr "Mentés"

#: src/qml/NewEvent.qml:435
msgid "Error"
msgstr "Hiba"

#: src/qml/NewEvent.qml:437
msgid "OK"
msgstr "OK"

#: src/qml/NewEvent.qml:499
msgid "From"
msgstr "-tól"

#: src/qml/NewEvent.qml:524
msgid "To"
msgstr "-ig"

#: src/qml/NewEvent.qml:553
msgid "All day event"
msgstr "Egész napos esemény"

#: src/qml/NewEvent.qml:590
msgid "Event Name"
msgstr "Esemény neve"

#: src/qml/NewEvent.qml:610
msgid "More details"
msgstr "Több részlet"

#: src/qml/NewEvent.qml:661
msgid "Location"
msgstr "Helyszín"

#: src/qml/NewEvent.qml:749
msgid "Guests"
msgstr "Vendégek"

#: src/qml/NewEvent.qml:759
msgid "Add Guest"
msgstr "Vendég hozzáadása"

#: src/qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "A létrehozáshoz válasszon fiókot."

#: src/qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Egyszeri"

#: src/qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Naponta"

#: src/qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "Hétköznapokon"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: src/qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "%1, %2 és %3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: src/qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "%1 és %2"

#: src/qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Hetente"

#: src/qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Havonta"

#: src/qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Évente"

#: src/qml/RemindersModel.qml:31 src/qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Nincs emlékeztető"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: src/qml/RemindersModel.qml:34 src/qml/RemindersModel.qml:103
msgid "On Event"
msgstr "Esemény idejekor"

#: src/qml/RemindersModel.qml:43 src/qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 hét"
msgstr[1] "%1 hét"

#: src/qml/RemindersModel.qml:54 src/qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 nap"
msgstr[1] "%1 nap"

#: src/qml/RemindersModel.qml:65 src/qml/RemindersModel.qml:108
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 óra"
msgstr[1] "%1 óra"

#: src/qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 perc"
msgstr[1] "%1 perc"

#: src/qml/RemindersModel.qml:104 src/qml/RemindersModel.qml:105
#: src/qml/RemindersModel.qml:106 src/qml/RemindersModel.qml:107
#: src/qml/SettingsPage.qml:304 src/qml/SettingsPage.qml:305
#: src/qml/SettingsPage.qml:306 src/qml/SettingsPage.qml:307
#: src/qml/SettingsPage.qml:308 src/qml/SettingsPage.qml:309
#: src/qml/SettingsPage.qml:310 src/qml/SettingsPage.qml:311
msgid "%1 minutes"
msgstr "%1 perc"

#: src/qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr "%1 óra"

#: src/qml/RemindersModel.qml:111
msgid "%1 days"
msgstr "%1 nap"

#: src/qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr "%1 hét"

#: src/qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Egyéni"

#: src/qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Egyedi emlékeztető"

#: src/qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr "Emlékeztető beállítása"

#: src/qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "Hetek számának megjelenítése"

#: src/qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr "Kínai naptár mutatása"

#: src/qml/SettingsPage.qml:125
msgid "Business hours"
msgstr "Munkaidő"

#: src/qml/SettingsPage.qml:235
msgid "Default reminder"
msgstr "Alapértelmezett emlékeztető"

#: src/qml/SettingsPage.qml:282
msgid "Default length of new event"
msgstr "Új esemény alapértelmezett időtartalma"

#: src/qml/SettingsPage.qml:345
msgid "Default calendar"
msgstr "Alapértelmezett naptár"

#: src/qml/SettingsPage.qml:414
msgid "Theme"
msgstr "Téma"

#: src/qml/SettingsPage.qml:437
msgid "System theme"
msgstr "Rendszer témája"

#: src/qml/SettingsPage.qml:438
msgid "SuruDark theme"
msgstr "SuruDark téma"

#: src/qml/SettingsPage.qml:439
msgid "Ambiance theme"
msgstr "Ambiance Téma"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: src/qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "%1. hét"

#: src/qml/WeekView.qml:147 src/qml/WeekView.qml:148
msgid "MMM"
msgstr "MMM"

#: src/qml/YearView.qml:83
msgid "Year %1"
msgstr "%1. év"

#: src/qml/calendar.qml:102
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"A naptár alkalmazás négy argumentumot fogad el: --starttime, --endtime, --"
"newevent és --eventid. Ezeket a rendszer fogja kezelni. A hozzájuk tartozó "
"összes megjegyzéshez nézze meg a forrást."

#: src/qml/calendar.qml:394 src/qml/calendar.qml:580
msgid "Day"
msgstr "Nap"

#: src/qml/calendar.qml:402 src/qml/calendar.qml:593
msgid "Week"
msgstr "Hét"

#: src/qml/calendar.qml:410 src/qml/calendar.qml:606
msgid "Month"
msgstr "Hónap"

#: src/qml/calendar.qml:418 src/qml/calendar.qml:619
msgid "Year"
msgstr "Év"

#~ msgid "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"
#~ msgstr "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"

#~ msgid "no event name set"
#~ msgstr "nincs eseménynév megadva"

#~ msgid "no location"
#~ msgstr "nincs helyszín megadva"

#~ msgid "Data refresh interval"
#~ msgstr "Adatfrissítési intervallum"

#~ msgid ""
#~ "Note: Automatic syncronisation currently only works while the app is open "
#~ "in the foreground. Alternatively set background suspension to off. Then "
#~ "sync will work while the app is open in the background."
#~ msgstr ""
#~ "Megjegyzés: Jelenleg az automatikus szinkronizáció csak akkor működik, ha "
#~ "az alkalmazás meg van nyitva. Alternatív megoldás a háttérben történő "
#~ "alkalmazásfelfüggesztés kikapcsolása. Ebben az esetben a szinkronizálás "
#~ "akkor is működni fog, mikor az alkalmazás a háttérben van megnyitva."

#~ msgid "5 minutes"
#~ msgstr "5 perc"

#~ msgid "10 minutes"
#~ msgstr "10 perc"

#~ msgid "15 minutes"
#~ msgstr "15 perc"

#~ msgid "30 minutes"
#~ msgstr "30 perc"

#~ msgid "1 hour"
#~ msgstr "1 óra"

#~ msgid "2 hours"
#~ msgstr "2 óra"

#~ msgid "1 day"
#~ msgstr "1 nap"

#~ msgid "2 days"
#~ msgstr "2 nap"

#~ msgid "1 week"
#~ msgstr "1 hét"

#~ msgid "2 weeks"
#~ msgstr "2 hét"
